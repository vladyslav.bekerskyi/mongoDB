const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getMe,
  deleteMe,
  changePassword,
} = require('../controllers/userController');
const {protect} = require('../middleware/authMiddleware');
router
    .route('/me')
    .get(protect, getMe)
    .delete(protect, deleteMe)
    .patch(protect, changePassword);

module.exports = router;
