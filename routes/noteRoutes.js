const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getNotes,
  setNote,
  updateNote,
  deleteNote,
  getNote,
  checkNote,
} = require('../controllers/noteController');
const {protect} = require('../middleware/authMiddleware');
router.route('/').get(protect, getNotes).post(protect, setNote);
router
    .route('/:id')
    .put(protect, updateNote)
    .delete(protect, deleteNote)
    .get(protect, getNote)
    .patch(protect, checkNote);

// router.get('/',getNotes)
// router.post('/',setNote)
// router.put('/:id',putNote)
// router.delete('/:id',deleteNote)

module.exports = router;
