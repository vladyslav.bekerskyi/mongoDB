const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const noteSchema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      completed: {
        type: Boolean,
      },
      text: {
        type: String,
        required: [true, 'Please add a text value'],
      },
      createdDate: {
        type: String,
        required: [true, 'Please add a text value'],
      },
    },
);

module.exports = mongoose.model('Note', noteSchema);
