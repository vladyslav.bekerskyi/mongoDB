const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const userSchema = mongoose.Schema(
    {
      username: {
        type: String,
        required: [true, 'Please add a name'],
        unique: true,
      },
      password: {
        type: String,
        required: [true, 'Please add a password'],
      },
    },
    {
      timestamps: true,
    },
);

module.exports = mongoose.model('User', userSchema);
