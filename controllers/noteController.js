const asyncHandler = require('express-async-handler');
const Note = require('../models/noteModel');

const getNotes = asyncHandler(async (req, res) => {
  const {offset, limit} = req.query;
  const notes = await Note.find({
    userId: req.user.id,
  })
      .skip(offset || 0)
      .limit(limit || 0)
      .select('-__v');

  res.status(200).json({
    offset: offset || 0,
    limit: limit || 0,
    count: notes.length,
    notes: notes,
  });
});

const setNote = asyncHandler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!req.body.text) {
    res.status(400);
    throw new Error('Please add a text field');
  }

  await Note.create({
    userId: req.user.id,
    completed: false,
    text: req.body.text,
    createdDate: new Date().toISOString(),
  });

  res.status(200).json({
    message: 'Success',
  });
});

const updateNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json({
    message: 'Success',
  });
});

const deleteNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Goal not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.findByIdAndDelete(req.params.id);
  res.status(200).json({
    message: 'Success',
  });
});

const getNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    note: {
      _id: note._id,
      userId: note.userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdAt,
    },
  });
});

const checkNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  note.completed = !note.completed;

  await Note.findByIdAndUpdate(req.params.id, note, {
    new: true,
  });
  res.status(200).json({
    message: 'Success',
  });
});

module.exports = {
  getNotes,
  setNote,
  updateNote,
  deleteNote,
  getNote,
  checkNote,
};
